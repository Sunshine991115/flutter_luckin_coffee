import 'package:flutter/material.dart';
import 'package:flutter_luckin_coffee/pages/app/home/index.dart';
import 'package:flutter_luckin_coffee/pages/app/menu/index.dart';
import 'package:flutter_luckin_coffee/pages/app/mine/index.dart';
import 'package:flutter_luckin_coffee/pages/app/order/index.dart';
import 'package:flutter_luckin_coffee/pages/app/shopping_cart/index.dart';
import 'package:share_extend/share_extend.dart';

class Main extends StatefulWidget {
  int cuttentIndex = 0;

  Main({@required int cuttentIndex, Key key}) : super(key: key) {
    this.cuttentIndex = cuttentIndex;
  }

  @override
  State<Main> createState() => _MainState(cuttentIndex: cuttentIndex);
}

class _MainState extends State<Main> {
  PageController _pageController;

  int cuttentIndex = 0;

  _MainState({@required int cuttentIndex}) {
    this.cuttentIndex = cuttentIndex;
    _pageController = PageController(initialPage: this.cuttentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            "首页",
            style: TextStyle(
                color: Color.fromRGBO(56, 56, 56, 1),
                fontSize: 18,
                fontWeight: FontWeight.bold),
          )),
        floatingActionButton: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          color: Color.fromRGBO(253, 56, 56, 1),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8)),
            child: Text('购买'),
          ),
        ),
      drawer: Container(
        margin: EdgeInsets.only(right: 200),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8), topRight: Radius.circular(8)),
          child: Mine(),
          ),
        ),
      persistentFooterButtons: [Center(child: Text('欢迎购买瑞幸咖啡'))],
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Home(),
          Menu(),
          Order(),
          ShoppingCart(),
          Mine(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Color.fromRGBO(43, 76, 126, 1),
        unselectedItemColor: Color.fromRGBO(43, 76, 126, .5),
        currentIndex: cuttentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() {
            _pageController.jumpToPage(index);
            cuttentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu_book),
            label: '菜单',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article_rounded),
            label: '订单',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_cart,
            ),
            label: '购物车',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '我的',
          ),
        ],
      ),
    );
  }
}
